import { Connection, Channel, connect } from "amqplib";

console.log("receiver startup");

let conn: Connection;
let channel: Channel;
const queueConfig = {durable: true, autoDelete: false, exclusive: false, maxPriority: 2};
const exchangeconfig = {durable: true, autoDelete: false, arguments: {'x-delayed-type':  "topic"}};
const exchangeName = "DEV.nowjobs.delayed-topic";
const queueName = "DEV.nowjobs.notis";
const messageType = "noti";
const messageTopic = "vaca";

async function wait(sec) {
  return new Promise(resolve => setTimeout(resolve, sec*1000));
}

async function connectRabbit() {
  conn = await connect('amqp://localhost');
  channel = await conn.createChannel();
  channel.prefetch(2, false);
  await channel.assertExchange(exchangeName, 'x-delayed-message', exchangeconfig);
  const queue = await channel.assertQueue(queueName, queueConfig);
  await channel.bindQueue(queueName, exchangeName, messageType+".*");
  console.log("current queue state: ", queue)
}


let nrConsumers = 0;

async function receiveMessages(key: string, consumer) {
  await channel.consume(queueName, (msg) => {
    nrConsumers++;
    consumer(msg.fields.exchange, msg.fields.routingKey, msg.content, async (ack: boolean) => {
      if (ack) {
        channel.ack(msg); 
        nrConsumers--;
      }
    });
  });
}

async function run() {
  try {
    await connectRabbit();
    await receiveMessages("*", async (exchange, topic, msg, ack) => { 
      console.log("current queue state: ", await channel.checkQueue(queueName));
      console.log(nrConsumers + " - received '" + topic + "' from '" + queueName + "' by '" + exchange + "' -> '" + msg.toString() + "'");
      await wait(0.001);
      ack(true);
    });
  } catch(e) {
    conn.close();
    console.log(e);
  }
}

run();

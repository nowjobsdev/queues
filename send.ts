import { Channel, Connection, connect } from "amqplib";

console.log("sender startup");

enum Delivery { persistent = 2, transient = 1 };
enum Priority { low = 0, high = 1}

let conn: Connection;
let channel: Channel;
const queueConfig = {durable: true, autoDelete: false, exclusive: false, maxPriority: 2};
const exchangeconfig = {durable: true, autoDelete: false, arguments: {'x-delayed-type':  "topic"}};
const exchangeName = "DEV.nowjobs.delayed-topic";
const queueName = "DEV.nowjobs.notis";
const messageType = "noti";
const messageTopic = "vaca";

async function wait(sec) {
  return new Promise(resolve => setTimeout(resolve, sec*1000));
}

async function connectRabbit() {
  conn = await connect('amqp://localhost');
  channel = await conn.createChannel();
  channel.prefetch(2, false);
  await channel.assertExchange(exchangeName, 'x-delayed-message', exchangeconfig);
  const queue = await channel.assertQueue(queueName, queueConfig);
  await channel.bindQueue(queueName, exchangeName, messageType+".*");
  console.log("current queue state: ", queue)
}

async function sendMessage(key: string, payload: any, priority = Priority.low, delay: number) {
  const str = JSON.stringify(payload);
  const topic = messageType+"."+key;
  console.log("sending '" + topic + "' over '" + exchangeName + "' -> '" + str + "'");
  return channel.publish(exchangeName, topic, Buffer.from(str), 
    {persistent: true, deliveryMode: Delivery.persistent, priority, contentType: 'application/json',
     headers: {"x-delay": delay * 1000}}
  );
}

async function run() {
  try {
    await connectRabbit();
    for (let i = 1; i<=10; i++) {
      await sendMessage(messageTopic, {task: i, prio: i%2, delay: (i%3)*3}, i % 2, (i%3)*3);
    }
    setTimeout( () => {
      console.log("closing connection");
      conn.close();
    }, 10000);
} catch(e) {
    console.log(e);
    conn.close();
  }
}

run();